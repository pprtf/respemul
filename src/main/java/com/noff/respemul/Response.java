package com.noff.respemul;
import lombok.Data;
import lombok.Builder;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

@Data
@Builder
public class Response {

	@JsonProperty("uniq_req_id")
	String id;
	RespData data;

	@Data
	@Builder
	public static class RespData {
		@JsonProperty("unit_id")
		String unitId;
		@JsonProperty("display_time")
		String displayTime;
		Audience audience;
	}

	@Data
	@Builder
	public static class Audience {
		@JsonProperty("total_reach")
		Integer totalReach;
		@JsonProperty("total_floor")
		Float totalFloor;
		@JsonProperty("total_floor_preferred_deal")
		Float totalFloorPD;
		@JsonProperty("total_floor_low_priority_deal")
		Float totalFloorLPD;
		Integer macs;
		Integer ots;
		List<Segment> segments; 
	}

	@Data
	@Builder
	public static class Segment {
		Integer id;
		Integer reach;
		@JsonProperty("target_floor")
		Float targetFloor;
	}
}