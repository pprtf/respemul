package com.noff.respemul;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import com.noff.respemul.Response.RespData;
import com.noff.respemul.Response.Audience;
import com.noff.respemul.Response.Segment;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import io.swagger.annotations.Api; 
import io.swagger.annotations.ApiOperation; 

@RestController
@Slf4j
@Api(description = "Respemul Controller description") //swagger
public class Controller {
	@ApiOperation("Get segmenst")
	@GetMapping("/get_segments")
	public Response getSegments(@RequestParam("frame_id") String frameId,
                            	@RequestParam("display_time") String displayTime,
                            	@RequestParam("uniq_req_id") String uniqReqId) {

		List<Segment> segments = new ArrayList<>();
		int segN = (int)(5 + (Math.random()*5));
		for (int i=0;i<segN;i++) {
			int id = (int)(5 + (Math.random()*5));
			int reach = (int)(Math.random()*50);
			float tgFloor = ((int)(1 + (Math.random()*5)))/2f;
			segments.add(new Segment(id,reach,tgFloor));
		}

		int tr = (int)(Math.random()*50);
		float tgFloor = ((int)(1 + (Math.random()*50)))/2f;
		float tgFloorPD = ((int)(1 + (Math.random()*5)))/2f;
		float tgFloorLPD = ((int)(1 + (Math.random()*5)))/2f;
		int macs = (int)(1 + Math.random()*50);
		int ots = (int)(1 + Math.random()*50);

		Response resp = Response.builder()
			.id(uniqReqId)
			.data(RespData.builder()
			.unitId(frameId)
			.displayTime(displayTime)
			.audience(Audience.builder()
				.totalReach(tr)
				.totalFloor(tgFloor)
				.totalFloorPD(tgFloorPD)
				.totalFloorLPD(tgFloorLPD)
				.macs(macs)
				.ots(ots)
				.segments(segments)
				.build())
			.build())
		.build();

		log.info("Response generated: {}", resp);

		return resp;
	}
}