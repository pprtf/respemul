FROM maven:3.6.0-jdk-11-slim AS TEMP_BUILD_IMAGE
RUN mkdir -p /home/app
WORKDIR /home/app/
COPY . .
RUN mvn clean package -DskipTests=true


FROM openjdk:11-jre-slim
ENV ARTIFACT_NAME=respemul-0.0.1-SNAPSHOT.jar
RUN mkdir -p /home/app
WORKDIR /home/app/
COPY --from=TEMP_BUILD_IMAGE /home/app/target/$ARTIFACT_NAME .

EXPOSE 8081
ENTRYPOINT ["java", "-jar", "./respemul-0.0.1-SNAPSHOT.jar"]